package ro.tuc.pt.tema2;

import java.util.*;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class Service implements Runnable {

	private int minServiceTime;
	private int maxServiceTime;
	private int minArrivalTime;
	private int maxArrivalTime;
	private int noOfQueues;
	private int noOfCustomers;
	private int simulationTime;

	public List<Queue> coada = new ArrayList<Queue>();

	public Service(int minServiceTime, int maxServiceTime, int minArrivalTime, int maxArrivalTime, int noOfQueues,
			int noOfCustomers, int simulationTime) {

		this.minServiceTime = minServiceTime;
		this.maxServiceTime = maxServiceTime;
		this.minArrivalTime = minArrivalTime;
		this.maxArrivalTime = maxArrivalTime;
		this.noOfQueues = noOfQueues;
		this.noOfCustomers = noOfCustomers;
		this.simulationTime = simulationTime;

		for (int i = 0; i < 5; i++) {
			coada.add(new Queue());
		}
	}

	public int getMinimumQueue() {

		int min = 15;
		int i = 0;
		for (i = 0; i < 5; i++) {
			if (coada.get(i).getCoadaClienti().size() <= min) {
				min = i;
			}
		}
		return min;
	}

	public void simulation() throws InterruptedException {

		float arrivalTime;
		int serviceTime;
		int customerID;

		for (customerID = 1; customerID <= this.noOfCustomers; customerID++) {
			arrivalTime = this.minArrivalTime + new Random().nextInt(this.maxArrivalTime - this.minArrivalTime);
			Thread.sleep((long) arrivalTime);
			serviceTime = this.minServiceTime + new Random().nextInt(this.maxServiceTime - this.minServiceTime);
			Client client = new Client(customerID, arrivalTime, serviceTime);
			int numberList = getMinimumQueue();
			coada.get(numberList).addClient(client);
			System.out.println(client);
		}
	}

	public void run() {
		long start = System.currentTimeMillis();
		// TODO Auto-generated method stub
		try {
			simulation();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
