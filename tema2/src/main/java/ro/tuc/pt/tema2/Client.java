package ro.tuc.pt.tema2;

public class Client {

	private int idClient;
	private float arrivalTime;
	private int serviceTime;

	public Client(int idClient, float arrivalTime, int serviceTime) {

		this.setIdClient(idClient);
		this.setArrivalTime(arrivalTime);
		this.setServiceTime(serviceTime);
	}

	public int getIdClient() {
		return idClient;
	}

	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}

	public float getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(float arriveTime) {
		this.arrivalTime = arriveTime;
	}

	public int getServiceTime() {
		return serviceTime;
	}

	public void setServiceTime(int serviceTime) {
		this.serviceTime = serviceTime;
	}

	synchronized public String toString() {
		return "Client " + this.idClient + " arrived AR = " + this.arrivalTime;
	}
}
