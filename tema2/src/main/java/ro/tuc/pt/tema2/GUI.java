package ro.tuc.pt.tema2;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingWorker;

public class GUI {

	private JFrame frame;
	private ArrayList<JTextArea> checkout = new ArrayList<JTextArea>();
	private JTextField minim = new JTextField();
	private JTextField maxim = new JTextField();
	private JTextField minServiceTime = new JTextField();
	private JTextField maxServiceTime = new JTextField();
	private JTextField timeForSimulate = new JTextField();
	private JTextField noOfCustomers = new JTextField();
	public Service serv;
	public Queue qu;

	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					window.metodaGUI();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

	public void metodaGUI() {
		// TODO Auto-generated method stub

		frame = new JFrame();
		frame.setTitle("Shop");
		frame.setBounds(150, 120, 915, 515);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setVisible(true);

		checkout.add(new JTextArea());
		checkout.get(0).setBounds(10, 150, 170, 300);
		checkout.get(0).setEditable(false);
		checkout.add(new JTextArea());
		checkout.get(1).setBounds(185, 150, 170, 300);
		checkout.get(1).setEditable(false);
		checkout.add(new JTextArea());
		checkout.get(2).setBounds(360, 150, 170, 300);
		checkout.get(2).setEditable(false);
		checkout.add(new JTextArea());
		checkout.get(3).setBounds(535, 150, 170, 300);
		checkout.get(3).setEditable(false);
		checkout.add(new JTextArea());
		checkout.get(4).setBounds(710, 150, 170, 300);
		checkout.get(4).setEditable(false);

		frame.add(checkout.get(0));
		frame.add(checkout.get(1));
		frame.add(checkout.get(2));
		frame.add(checkout.get(3));
		frame.add(checkout.get(4));

		JLabel minL = new JLabel();
		minL.setBounds(10, 10, 80, 20);
		minL.setText("minTime:");
		frame.add(minL);

		minim.setBounds(70, 10, 55, 20);
		frame.add(minim);

		JLabel maxL = new JLabel();
		maxL.setBounds(10, 50, 80, 20);
		maxL.setText("maxTime:");
		frame.add(maxL);

		maxim.setBounds(70, 50, 55, 20);
		frame.add(maxim);

		JLabel minServiceTLabel = new JLabel();
		minServiceTLabel.setBounds(150, 10, 80, 20);
		minServiceTLabel.setText("minService:");
		frame.add(minServiceTLabel);

		minServiceTime.setBounds(225, 10, 55, 20);
		frame.add(minServiceTime);

		JLabel maxServiceTLabel = new JLabel();
		maxServiceTLabel.setBounds(150, 50, 80, 20);
		maxServiceTLabel.setText("maxService:");
		frame.add(maxServiceTLabel);

		maxServiceTime.setBounds(225, 50, 55, 20);
		frame.add(maxServiceTime);

		JLabel timeL = new JLabel();
		timeL.setBounds(300, 10, 80, 20);
		timeL.setText("Simulation:");
		frame.add(timeL);

		timeForSimulate.setBounds(370, 10, 80, 20);
		frame.add(timeForSimulate);

		JLabel clienti = new JLabel();
		clienti.setBounds(310, 50, 80, 20);
		clienti.setText("nrClienti:");
		frame.add(clienti);

		noOfCustomers.setBounds(370, 50, 80, 20);
		frame.add(noOfCustomers);

		JButton startGenerate = new JButton();
		startGenerate.setBounds(550, 10, 100, 20);
		startGenerate.setText("Start");
		frame.add(startGenerate);

		startGenerate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!minServiceTime.getText().isEmpty())
					(new Worker()).execute();
			}
		});
	}

	class Worker extends SwingWorker<String, String> {

		@Override
		protected String doInBackground() throws Exception {
			
			int timpServMin = Integer.parseInt(minServiceTime.getText());
			int timpServMax = Integer.parseInt(maxServiceTime.getText());
			int timpMinim = Integer.parseInt(minim.getText());
			int timpMaxim = Integer.parseInt(maxim.getText());
			int nrCust = Integer.parseInt(noOfCustomers.getText());
			int simTime = Integer.parseInt(timeForSimulate.getText());

			serv = new Service(timpServMin, timpServMax, timpMinim, timpMaxim, 5, nrCust, simTime);
			
			Thread t1 = new Thread(new Runnable() {
				public void run() {
					try {
						serv.simulation();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}); // porneste simularea


			Thread queueContent = new Thread(new Runnable() {
				public void run() {
					while (true)
						for (int i = 0; i < 5; i++) {
								checkout.get(i).setText(serv.coada.get(i).toString());	
						}
				}
			});

			ExecutorService executor = Executors.newFixedThreadPool(10);
			executor.execute(t1);
			executor.execute(queueContent);
			executor.execute(serv.coada.get(0));
			executor.execute(serv.coada.get(1));
			executor.execute(serv.coada.get(2));
			executor.execute(serv.coada.get(3));
			executor.execute(serv.coada.get(4));
			return null;
		}

	}
}
