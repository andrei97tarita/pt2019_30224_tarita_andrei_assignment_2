package ro.tuc.pt.tema2;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class Queue extends Thread {

	BlockingQueue<Client> coadaClienti = new ArrayBlockingQueue<Client>(15);

	synchronized void addClient(Client client) {

		this.coadaClienti.offer(client);
	}

	synchronized int removeClient() {
		if (coadaClienti.isEmpty())
			return 0;
		Client c = coadaClienti.poll();
		System.out.println("Sters: " + c.getIdClient() + " ST = " + c.getServiceTime());
		return c.getServiceTime();
	}

	synchronized BlockingQueue<Client> getCoadaClienti() {
		return coadaClienti;
	}

	public void run() {
		// TODO Auto-generated method stub
		while (true) {
			try {
				int ST = removeClient();
				Thread.sleep(ST);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public String toString() {
		String coada = new String();
		for (Client c : coadaClienti)
			coada += c.toString() + "\n";
		return coada;
	}
}
